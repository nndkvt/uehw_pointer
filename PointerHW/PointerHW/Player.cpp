#include "Player.h"

Player::Player() : name(""), points(0)
{}

Player::Player(std::string _name) : name(_name), points(0)
{}

Player::Player(std::string _name, int _points) : name(_name), points(_points)
{}

Player::~Player() {}

std::string Player::GetName() { return name; }

int Player::GetPoints() { return points; }

void Player::DisplayInfo()
{
	std::cout << name << "\t | " << points << "\n";
}