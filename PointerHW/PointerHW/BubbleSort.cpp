#include "BubbleSort.h"

// Func for sorting arrays of Player objects
void BubbleSortPlayer(Player* arr, int size)
{
    bool b = true;
    while (b) {
        b = false;
        for (int i(0); i + 1 < size; i++) {
            // If an element's points are lower than the next ones,
            // we swap the elements and change 'b' to go through an array one more time
            if ((arr + i)->GetPoints() < (arr + i + 1)->GetPoints()) {
                std::swap(*(arr + i), *(arr + i + 1));
                b = true;
            }
        }
    }
}