#pragma once

#include <iostream>
#include <string>

class Player
{
private:
	std::string name;
	int points;
public:
	Player();
	Player(std::string _name);
	Player(std::string _name, int points);
	~Player();
	std::string GetName();
	int GetPoints();
	void DisplayInfo();
};