﻿#include "Player.h"
#include "BubbleSort.h"

int main()
{
    int n;

    std::cout << "Enter the number of players: ";
    std::cin >> n;

    Player* players = new Player[n];

    for (int i = 0; i < n; i++) 
    {
        std::string _name;
        std::cout << "Enter #" << i + 1 << " player\'s name: ";
        std::cin >> _name;

        int _points;
        std::cout << "Enter #" << i + 1 << " player\'s points: ";
        std::cin >> _points;

        *(players + i) = Player(_name, _points);
        std::cout << "Player #" << i + 1 << " was added\n\n";
    }

    BubbleSortPlayer(players, n);

    for (int i = 0; i < n; i++)
    {
        (players + i)->DisplayInfo();
    }

    delete[n] players;

    return 0;
}
